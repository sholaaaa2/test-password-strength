import React, { useState } from 'react';

const App = () => {
  const [password, setPassword] = useState('');
  const [passwordStrength, setPasswordStrength] = useState(''); // "short", "weak", "medium", "strong"
  const [passwordVisible, setPasswordVisible] = useState(false);

  const handlePasswordChange = (event) => {
    const newPassword = event.target.value;
    setPassword(newPassword);
    calculatePasswordStrength(newPassword);
  };

  const calculatePasswordStrength = (password) => {
    if (password === '') {
      setPasswordStrength('');
    } else if (password.length < 8) {
      setPasswordStrength('short');
    } else if (
      /[a-zA-Z]/.test(password) &&
      /[0-9]/.test(password) &&
      /[^a-zA-Z0-9]/.test(password)
    ) {
      setPasswordStrength('strong');
    }else if (
      /^[a-zA-Z]+$/.test(password) ||
      /^[0-9]+$/.test(password) ||
      /^[^a-zA-Z0-9]+$/.test(password)
    ) {
      setPasswordStrength('weak');
    } else {
      setPasswordStrength('medium');
    }
  };

  const getSectionColor = (section) => {
    if (password === '') {
      return 'gray';
    }
    if (passwordStrength === 'short') {
      return 'red';
    }
    if (passwordStrength === 'weak') {
      return section === 1 ? 'red' : 'gray';
    }
    if (passwordStrength === 'medium') {
      return section <= 2 ? 'yellow' : 'gray';
    }
    if (passwordStrength === 'strong') {
      return 'green';
    }
  };
  const togglePasswordVisibility = () => {
    setPasswordVisible(!passwordVisible);
  };

  return (
    <div className="App">
      <h1>Password Strength Checker</h1>
      <div className="input-container">
        <input
          type={passwordVisible ? 'text' : 'password'}
          placeholder="Enter your password"
          value={password}
          onChange={handlePasswordChange}
        />
        <button onClick={togglePasswordVisibility}>
          {passwordVisible ? 'Hide' : 'Show'}
        </button>
      </div>
      <div className="strength-sections">
        <div
          className="strength-section"
          style={{ backgroundColor: getSectionColor(1) }}
        ></div>
        <div
          className="strength-section"
          style={{ backgroundColor: getSectionColor(2) }}
        ></div>
        <div
          className="strength-section"
          style={{ backgroundColor: getSectionColor(3) }}
        ></div>
      </div>
    </div>
  );
};

export default App;